﻿using System;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Server
{
    public partial class Form1 : Form
    {
        private BackgroundWorker worker = new BackgroundWorker();

        public Form1()
        {
            InitializeComponent();
            //Thread thread = new Thread(new ThreadStart(start));
            //start();
            //thread.Start();
            
            worker.DoWork += Receiver_DoWork;
            worker.ProgressChanged += Receiver_ProgressChanged;
            worker.RunWorkerAsync();
        }

        private void Receiver_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtMessage.AppendText(e.UserState.ToString() + "\n");
        }

        private void Receiver_DoWork(object sender, DoWorkEventArgs e)
        {
            //BackgroundWorker worker = sender as BackgroundWorker;
            while (true)
            {
                var factory = new ConnectionFactory() { Uri = new Uri(@"amqp://tamkowtd:-uFwWjan0NOXwxVM8eWumPVpgOZhxzUb@termite.rmq.cloudamqp.com/tamkowtd") };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: "task_queue", durable: false, exclusive: false, autoDelete: false, arguments: null);

                        var consumer = new EventingBasicConsumer(channel);
                        consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body;
                            var message = Encoding.UTF8.GetString(body);
                            worker.ReportProgress(0, message);
                            //this.Invoke(() => this.txtMessage.Text = message.ToString(); );
                            int dots = message.Split('.').Length - 1;
                            Thread.Sleep(dots * 1000);

                            channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                        };
                        channel.BasicConsume(queue: "task_queue", autoAck: false, consumer: consumer);

                    }
                }
            }

        }

        private void start()
        {

            //txtMessage.Text = " Ready";
            var factory = new ConnectionFactory() { Uri = new Uri(@"amqp://tamkowtd:-uFwWjan0NOXwxVM8eWumPVpgOZhxzUb@termite.rmq.cloudamqp.com/tamkowtd") };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                while (true)
                {
                    channel.QueueDeclare(queue: "task_queue", durable: false, exclusive: false, autoDelete: false, arguments: null);

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine(" [x] Received {0}", message);
                        //this.Invoke(() => this.txtMessage.Text = message.ToString(); );
                        int dots = message.Split('.').Length - 1;
                        Thread.Sleep(dots * 1000);

                        Console.WriteLine("[x] Done!");
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    };
                    channel.BasicConsume(queue: "task_queue", autoAck: false, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    //Console.ReadLine();
                }
            }
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            //if (btnStart.Text.Equals("Start"))
            //{
            //    var factory = new ConnectionFactory() { HostName = "localhost" };
            //    using (var connection = factory.CreateConnection())
            //    using (var channel = connection.CreateModel())
            //    {
            //        channel.QueueDeclare(queue: "task_queue",
            //                             durable: false,
            //                             exclusive: false,
            //                             autoDelete: false,
            //                             arguments: null);

            //        channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            //        txtMessage.Text = " [*] Waiting for messages.";

            //        var consumer = new EventingBasicConsumer(channel);
            //        consumer.Received += (model, ea) =>
            //        {
            //            var body = ea.Body;
            //            var message = Encoding.UTF8.GetString(body);
            //            txtMessage.Text = " [x] Received " + message;

            //            int dots = message.Split('.').Length - 1;
            //            Thread.Sleep(dots * 1000);

            //            txtMessage.Text = " [x] Done";

            //            channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            //        };
            //        channel.BasicConsume(queue: "task_queue",
            //                             autoAck: false,
            //                             consumer: consumer);

            //        txtMessage.Text = " Press [enter] to exit.";
            //    }
            //} else
            //{

            //}



        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

        }
    }
}