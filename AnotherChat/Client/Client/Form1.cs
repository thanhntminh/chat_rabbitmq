﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            var factory = new ConnectionFactory() { Uri = new Uri(@"amqp://tamkowtd:-uFwWjan0NOXwxVM8eWumPVpgOZhxzUb@termite.rmq.cloudamqp.com/tamkowtd") };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "task_queue", durable: false, exclusive: false, autoDelete: false, arguments: null);

                var message = txtMessage.Text;
                var body = Encoding.UTF8.GetBytes(message);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;
                //MessageBox.Show(message);
                channel.BasicPublish(exchange: "", routingKey: "task_queue", basicProperties: properties, body: body);
                //MessageBox.Show("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
                string s = txtMessageInfo.Text + "\n";
                txtMessageInfo.Text = s + " [x] Sent " + message;
                txtMessage.Text = "";
            }
            
        }

        
    }
}
